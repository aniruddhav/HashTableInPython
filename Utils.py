import datetime
from Student import Student

Courses = ['CSE', 'ECE', 'MEC','ARC' ]

class Utils:
    def findCurrentYear():
        now = datetime.datetime.now()
        #print (now.year)
        return now.year

    def generateKeys(tillYear):
        keySet = []
        beginYear = 2008
        #tillYear = ( Utils.findCurrentYear() - 5 )
        for year in range(beginYear, tillYear) :
            for course in Courses:
                generatedKey = str(year) + course
                #print("generatedKey : " + generatedKey)
                keySet.append(generatedKey)
        return keySet

    def generateKeysForCourse(tillYear, course):
        keySet = []
        beginYear = 2008
        #tillYear = ( Utils.findCurrentYear() - 1 )
        for year in range(beginYear, tillYear):
            generatedKey = str(year) + course
            keySet.append(generatedKey)
        return keySet

class PromptValues:
    thresholdCgpa = 0.0
    rangeMin = 0.0
    rangeMax = 5.0

    def __init__(self, thresholdCgpa = 0.0, rangeMin = 0.0, rangeMax = 5.0):
        self.thresholdCgpa = thresholdCgpa
        self.rangeMin = rangeMin
        self.rangeMax = rangeMax

    # getter method 
    def getThresholdCgpa(self): 
        return self.thresholdCgpa 
      
    # setter method 
    def setThresholdCgpa(self, thresholdCgpa): 
        self.thresholdCgpa = thresholdCgpa

    # getter method 
    def getMinRange(self): 
        return self.rangeMin 

    # setter method 
    def setMinRange(self, rangeMin): 
        self.rangeMin = rangeMin

    # getter method 
    def getMaxRange(self): 
        return self.rangeMax

    # setter method 
    def setMaxRange(self, rangeMax): 
        self.rangeMax = rangeMax

    def printDetails(self):
        print('Threshold CGPA : ' + str(self.getThresholdCgpa()))
        print('Min Range : ' + str(self.getMinRange()))
        print('Max Range : ' + str(self.getMaxRange()))
