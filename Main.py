from HashTable import HashTable
from Utils import Utils, PromptValues, Courses
from Student import Student

def main():
  promptValues = processPrompts()
  ht = populateHashTableFromFile("./resources/input/inputPS4.txt")
  
  hallOfFame(ht, promptValues.getThresholdCgpa())
  newCourseList(ht, promptValues.getMinRange(), promptValues.getMaxRange())
  depAvg(ht)
  destroyHash(ht)

# A function that reads from prompts file.
def processPrompts():
  f = open("./resources/input/promptsPS4.txt", "r")
  lines =f.readlines()
  f.close()

  promptValues = PromptValues()

  for line in lines :
    if( ':' in line):
      details = line.split(':')      
      tagName = details[0].strip()

      if tagName == 'hallOfFame':
        tagValue = details[1].strip()

        thresholdCgpa = float(tagValue)
        promptValues.setThresholdCgpa(thresholdCgpa)
        continue

      elif tagName == 'courseOffer':
        rangeMin = details[1].strip()
        rangeMax = details[2].strip()
        promptValues.setMaxRange(float(rangeMax))
        promptValues.setMinRange(float(rangeMin))

  promptValues.printDetails()
  return promptValues

def hallOfFame(hashTable, cgpa):
  fameList = []
  tillYear = Utils.findCurrentYear() - 1
  keys = Utils.generateKeys(tillYear)
  for key in keys:
    #print("keys : " + key )
    studentsInAClass = hashTable.getBucket(key)
    if studentsInAClass is not None and len(studentsInAClass) != 0:
      for aStudent in studentsInAClass:
        #aStudent.printDetails()
        if aStudent.cgpa > cgpa:
          fameOutputRow = aStudent.studentId + " / " + str(aStudent.cgpa)
          fameList.append( fameOutputRow )
          #print(fameOutputRow)

  f = open("./resources/output/outputPS4.txt", "a") 
  f.write("---------- hall of fame ----------" + "\n")
  f.write("Total eligible students : " + str(len(fameList)) + "\n")
  f.write("Qualified students:" + "\n")
  for line in fameList:
    f.write(line + "\n")
  f.write("-------------------------------------" + "\n")
  f.close()

def newCourseList(hashTable, cgpaFrom, cgpaTo):
  prospectiveStudentsList = []

  tillYear = Utils.findCurrentYear() - 5
  #print("tillYear : " + str(tillYear))
  keys = Utils.generateKeys(tillYear)
  for key in keys:
    #print("keys : " + key )
    studentsInAClass = hashTable.getBucket(key)
    if studentsInAClass is not None and len(studentsInAClass) != 0:
      for aStudent in studentsInAClass:
        if aStudent.cgpa > cgpaFrom and aStudent.cgpa < cgpaTo:
          prospectiveStudentRow = aStudent.studentId + " / " + str(aStudent.cgpa)
          prospectiveStudentsList.append( prospectiveStudentRow )
          #print(prospectiveStudentRow)

  f = open('./resources/output/outputPS4.txt', "a")
  f.write("\n\n\n")
  f.write("---------- new course candidates ----------" + "\n")
  f.write("Input: " + str(cgpaFrom) + " to " + str(cgpaTo) + "\n")
  f.write("Total eligible students : " + str(len(prospectiveStudentsList)) + "\n" )
  f.write("Qualified students:" + "\n")
  for line in prospectiveStudentsList:
    f.write(line + "\n")
  f.write("-------------------------------------" + "\n")
  f.close()

def depAvg(hashTable):
  resultList = []
  for course in Courses:
    resultList.append(depAvgAndMaxForGivenDept(hashTable, course))

  f = open("./resources/output/outputPS4.txt", "a")
  f.write("\n\n\n")
  f.write("---------- department CGPA ----------" + "\n")
  for line in resultList:
    f.write(line + "\n")
  f.write("-------------------------------------" + "\n")
  f.close()


def depAvgAndMaxForGivenDept(hashTable, courseName):
  #print("starting with course : " + courseName)
  max = 0.0 
  sum = 0.0
  avg = 0.0
  tillYear = ( Utils.findCurrentYear() - 1 )
  keys = Utils.generateKeysForCourse(tillYear, courseName)
  studentsCountInDept = 0
  for key in keys:
    #print("keys : " + key )
    studentsInAClass = hashTable.getBucket(key)
    if studentsInAClass is not None and len(studentsInAClass) != 0:
      for aStudent in studentsInAClass:
        if aStudent.cgpa > max:
          max = aStudent.cgpa

        sum = sum + aStudent.cgpa
        studentsCountInDept = studentsCountInDept + 1

  avg = sum / studentsCountInDept
  #print( "avg :" +  str(avg) )
  deptRow = courseName + " : max : " + str(max) + ", avg : " + str(avg)
  #print(deptRow)
  return deptRow

def destroyHash(hashTable):
  tillYear = ( Utils.findCurrentYear() - 1 )
  keys = Utils.generateKeys(tillYear)
  for key in keys:
    #print(key)
    studentsInAClass = hashTable.getBucket(key)
    if studentsInAClass is not None and len(studentsInAClass) != 0:
        studentsInAClass =  [None for studentsInAClass in range(len(studentsInAClass))]
        #print("Hashtable clean up completed values are as follows \n------" + str(studentsInAClass))


def insertStudentRec(hashTable, studentId, cgpa):
  student = Student(studentId, cgpa)
  hashTable.add(studentId, student)

def populateHashTableFromFile(file):
  ht = HashTable()
  f = open(file, "r")
  lines =f.readlines()
  for line in lines :
    if( '/' in line):
      details = line.split('/')
      key = details[0].strip()
      cgpa = details[1].strip()
      
      student = Student(key, cgpa)
      #student.printDetails()
      #ht.add(key, cgpa)
      insertStudentRec(ht, key, float(cgpa) )

  f.close()
  return ht

main()
