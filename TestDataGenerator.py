import random
from Utils import Utils
from Courses import Courses

def main():

    beginYear = 2008
    tillYear = ( Utils.findCurrentYear() - 1 )

    counter = 1
    for year in range(beginYear, tillYear) :
        for course in Courses:
            for counter in range(1, 100):
                key = ""

                if(counter < 10):
                    key = str(year) + str(course) + "000" + str(counter)
                elif(counter >= 10 and counter < 100):
                    key = str(year) + str(course) + "00" + str(counter)
                else:
                    key = str(year) + str(course) + "0" + str(counter)

                cgpa = random.randint(26, 50) / 10.0
                print(key + " / " + str(cgpa))
            
            print("\n")

main()