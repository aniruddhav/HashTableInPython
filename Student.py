
class Student:
    studentId = ""
    yearOfJoining = -1
    degreeProgram = ""
    rollNumber = -1
    cgpa = -1.1

    def __init__(self, studentId, cgpa):
        #print(len(studentId))
        if len(studentId) != 11:
            print("Invalid input : Skipping...")
            return

        try:
            tmp = studentId[0:4]
            self.yearOfJoining = int(tmp)

            self.degreeProgram = studentId[4:7]
            #check if one of the four courses

            tmp = studentId[7:11]
            self.rollNumber = int(tmp)

            self.studentId = studentId
            #print(yearOfJoining, degreeProgram, rollNumber)

            self.cgpa = cgpa
        except:
            print("Invalid input : EOW")

    def printDetails(self):
        print('Year Of Joining : ' + str(self.yearOfJoining) )
        print('Degree Program : ' + self.degreeProgram)
        print('Roll Number : ' + str(self.rollNumber))
        print('CGPA : ' + str(self.cgpa))
