
Hash Function:
----------------------
Hashtables are mainly meant for quickly searching a given object(or key) rather than iterating over it.
Ideal hash function is the one where there are no hash-collision and all buckets should contain only one object.
However, since we have finite number of buckets, a more practical hash-function will cause similar distribution
across all buckets and each bucket will contain more than one object ( as a result of collision )

University Problem:
------------------------
Here we are supposed to iterate over objects(CGPA values) and do some manipulation like average/min/max per department/course.
If we keep unique hash, then for N students there would be N buckets. And we will need to check for each
student to what department he/she belongs. This is not at all efficient, considering there would be thousands of students.

Instead, we can follow an approach like this -
There are 4 courses - ARC, ECE, MEC and CSE. For each year, there would be 4 buckets.
Assuming, 2008 as a starting point till 2028, we will have 20 * 4 = 80 buckets.

We can also have a fair assumption that each course can enroll upto 100 students. 
These will represent roll numbers.

So buckets will be something like -

2008CSE ----> 2.5 , 3.8, 4,2, ...
2008MEC ----> 2.6, 3.8, 4.1, 3.1, 4.7
2008ARC ----> 3.6, 4.9, ...
2008ECE ----> 3.5, 4.1, 4.2, 4.6
2008CSE ----> 2.5 , 3.8, 4,2
2008MEC ----> 2.6, 3.8, 4.1, 3.1, 4.7
2008ARC ----> 3.6, 4.9
2008ECE ----> 3.5, 4.1, 4.2, 4.6

