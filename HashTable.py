from Utils import Utils

class HashTable:
    def __init__(self):
        self.size = ( Utils.findCurrentYear() - 2008 ) * 4
        # set every cell to null
        self.buckets = [None] * self.size

    def hashId(self, key):
        hash = 0

        yearOfJoining = key[0:4]
        degreeProgram = key[4:7]
        rollNo = key[7:11]

        courseIndex = -1
        if degreeProgram == 'ECE':
            courseIndex = 0
        elif degreeProgram == 'MEC':
            courseIndex = 1
        elif degreeProgram == 'CSE':
            courseIndex = 2
        elif degreeProgram == 'ARC':
            courseIndex = 3

        hash = ( int(yearOfJoining) - 2008 ) * 4 + courseIndex
        #print("Key : " + key)
        #print("Generated Hash : " + str(hash))
        return hash

    def add(self, key, value):
        h1 = self.hashId(key)
        #key_value = [key, value]

        if self.buckets[h1] is None:
            self.buckets[h1] = list([value])
            return True
        else:
            for bucketItem in self.buckets[h1]:
                #if found existing --> update it & return
                if bucketItem.studentId == key:
                    bucketItem = value
                    return True
                #if bucketItem[0] == key:
                    #pair[1] = value
                    #return True

            #if not found --> add
            self.buckets[h1].append(value)
            return True

    def get(self, key):
        h1 = self.hashId(key)

        if self.buckets[h1] is not None:
            for pair in self.buckets[h1]:
                if pair[0] == key:
                    return pair[1]
        return

    def getBucket(self, key):
        h1 = self.hashId(key)
        if self.buckets[h1] is not None:
            return self.buckets[h1]

    def delete(self, key):
        h1 = self.hashId(key)

        if self.buckets[h1] is None:
            return False

        for i in range(0, len(self.buckets[h1])):
            obj = self.buckets[h1]

            if obj[i][0] == key:
                self.buckets[h1].pop(i)
                return True

    def print(self):
        print(str(len(self.buckets)))
        for item in self.buckets:
            print("Bucket : ")
            if item is not None:
                print(str(item))

